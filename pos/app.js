const status = document.getElementById('status');
const form = document.getElementById('form');
const barcode = document.getElementById('barcode');

const DEBUG_MODE = false;
const HTTP_HOST = DEBUG_MODE ? 'localhost' : '192.168.5.30';
const HTTP_PORT = 3080;
// const HTTPS_HOST = DEBUG_MODE ? 'localhost' : '192.168.5.30';
// const HTTPS_PORT = 3443;
const WSS_HOST = DEBUG_MODE ? 'localhost' : '192.168.5.30';
const WSS_PORT = 3443;

let ws;

let taskList = [];
let executorList = [];

window.onload = event => {
    try {
        ws = new WebSocket(`wss://${WSS_HOST}:${WSS_PORT}`);
    }
    catch (ex) {
        status.innerHTML = `Сервер обмена данными ОТКЛЮЧЕН. <a href="http://${HTTP_HOST}:${HTTP_PORT}">ПРОВЕРИТЬ ПОДКЛЮЧЕНИЕ</a>`;
        return;
    }
    barcode.focus();

    ws.onopen = () => setStatus('Сервер обмена данными подключен');
    ws.onclose = () => setStatus(`Сервер обмена данными ОТКЛЮЧЕН. <a href="http://${HTTP_HOST}:${HTTP_PORT}">ПРОВЕРИТЬ ПОДКЛЮЧЕНИЕ</a>`);
    ws.onmessage = response => {
        console.log(response.data);
        let task = new Task(JSON.parse(response.data));

        if (document.getElementById('task_' + task.data.order.barcode)) {
            task.dateOfChange = new Date();
            updatePickingTaskElement(task);
        }
        else {
            createPickingTaskElement(task);
        }
    }
}

const setStatus = value => {
    status.innerHTML = value
}

form.addEventListener('submit', event => {
    event.preventDefault()
    hideError()

    if (barcode.value.trim() === '') {
        showError('Штрихкод не может быть пустым.')
        barcode.value = ''
        return
    }

    if (document.getElementById('task_' + barcode.value)) {
        showError('Такой заказ уже есть в списке.')
        barcode.value = ''
        return
    }

    let task = createPickingTask(barcode.value)
    //createPickingTaskElement(task)

    //taskList.push(task)

    // ws.send(JSON.stringify({orderBarcode: barcode.value}))
    // ws.send(JSON.stringify(task))
    ws.send(JSON.stringify(task.toJSON()))
    barcode.value = ''
})

const createPickingTask = orderBarcode => {
    let task = new Task();
    task.setOrderBarcode(orderBarcode);

    return task;
}

const createPickingTaskElement = task => {
    let taskListDiv = document.getElementById('taskList')

    let order = task.data.order

    let taskDiv = document.getElementById('taskTemplate').cloneNode(true);
    taskDiv.id = 'task_' + order.barcode;
    taskDiv.style.display = 'block';

    taskDiv.querySelector('.task__dateOfCreation-value').innerHTML = task.getFormattedDateOfCreation();
    taskDiv.querySelector('.task__dateOfChange-value').innerHTML = task.getFormattedDateOfChange();
    taskDiv.querySelector('.task__operation-name').innerHTML = (task.operation === 'picking' ? 'Комплектация' : task.operation);
    taskDiv.querySelector('.task__status-name').innerHTML = getTaskStatusRussianName(task.status);
    taskDiv.querySelector('.task__order-number').innerHTML = order.barcode;
    taskDiv.querySelector('.task__executor-name').innerHTML = (task.executor.name === '' ? 'ПОКА НЕ НАЗНАЧЕН' : task.executor.name);
    if (
        task.executor
        && task.executor.name
    ) {
        taskDiv.querySelector('.task__executor-name').innerHTML = task.executor.name
    }

    // Статусы
    // new          - задача создана кассиром
    // inProgress   - задача выполняется кладовщиком
    // done         - задача завершена кладовщиком
    // canceled     - задача отменена

    if (task.status === 'new')
        taskDiv.style.backgroundColor = '#bbdefb';
    else if (task.status === 'done')
        taskDiv.style.backgroundColor = '#c8e6c9';
    else if (task.status === 'canceled')
        taskDiv.style.backgroundColor = '#ffcdd2';
    else
        taskDiv.style.backgroundColor = '#fff';

    taskListDiv.appendChild(taskDiv);
}

const updatePickingTaskElement = task => {
    let order = task.data.order

    let taskEl = document.getElementById('task_' + order.barcode)

    if (! taskEl) {
        return;
    }

    console.log(task);
    taskEl.querySelector('.task__dateOfChange-value').innerHTML = task.getFormattedDateOfChange();
    taskEl.querySelector('.task__status-name').innerHTML = getTaskStatusRussianName(task.status);
    taskEl.querySelector('.task__executor-name').innerHTML = task.executor.name;

    if (task.status === 'new')
        taskEl.style.backgroundColor = '#bbdefb'
    else if (task.status === 'done')
        taskEl.style.backgroundColor = '#c8e6c9'
    else if (task.status === 'canceled')
        taskEl.style.backgroundColor = '#ffcdd2'
    else
        taskEl.style.backgroundColor = '#fff'
}

const getTaskStatusRussianName = name => {
    switch (name) {
        case 'new': return 'новая'
        case 'inProgress': return 'в процессе'
        case 'canceled': return 'отменена'
        case 'done': return 'выполнена'
        default: return name
    }
}

const deletePickingTaskElement = task => {
    let order = task.data.order

    let taskEl = document.getElementById('task_' + order.barcode)

    if (! taskEl) {
        return
    }

    taskEl.parentNode.removeChild(taskEl)
}

const showError = message => {
    let errorEl = document.querySelector('.error')
    errorEl.innerHTML = message
    errorEl.style.display = 'block'
}

const hideError = () => {
    let errorEl = document.querySelector('.error')
    errorEl.innerHTML = ''
    errorEl.style.display = 'none'
}
